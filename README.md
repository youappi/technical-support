# Technical Support Software Projects #

####Device Detective
Device Detective by YouAppi is a free tool to fetch your device's details: device name (how it is known in the market), language, carrier, country, IP address, advertising ID and install referrer. No need to search through websites and device's settings - just launch the app and get all the details. Already available on Play Store.

####AppID Converter
Java project that validates the list of iTunes App Store tracking IDs, converts them to real app names, generates download links and saves them to a file. 
