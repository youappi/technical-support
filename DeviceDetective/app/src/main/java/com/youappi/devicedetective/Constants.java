package com.youappi.devicedetective;

/**
 * Created by boris on 2/13/2018.
 */

public interface Constants {
    String SIGNATURE = "Shared by Device Detective by YouAppi: https://play.google.com/store/apps/details?id=com.youappi.devicedetective";
    String ALLDATA = "%s\nLanguage: %s\nCarrier: %s\nCountry: %s\nIP: %s\nAdvertising ID:\n%s\nInstall Referrer:\n%s\n\n%s";
    String REFERRER_FORMAT = "%s\nInstall began on %s\nReferrer click happened on %s";
    String RESULT = "%s (fetch time: %sms)";
    String NOT_AVAILABLE = "not available";
}
