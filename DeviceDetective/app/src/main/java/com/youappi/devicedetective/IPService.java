package com.youappi.devicedetective;

import android.util.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.youappi.devicedetective.Constants.RESULT;

/**
 * Created by boris on 2/11/2018.
 * Service class to fetch device's IP.
 */

public class IPService {

    private String ip;
    public CallbackListener callbackListener;

    public void setCallbackListener(CallbackListener callbackListener){
        this.callbackListener = callbackListener;
    }

    /*
    Get user's public IP address.
     */
    public void getIP(){
        final long startTime = System.nanoTime();
        Log.v("IP", "Trying to get IP address");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Document doc = null;
                try {
                    doc = Jsoup.connect("http://www.checkip.org").get();
                } catch (IOException e) {
                    Log.e("IP", e.getMessage());
                }
                ip = doc.getElementById("yourip").select("h1").first().select("span").text();
                Log.w("IP", "IP=" + ip);
                final long delayNano = System.nanoTime() - startTime;
                int delayMs = (int) TimeUnit.MILLISECONDS.convert(delayNano, TimeUnit.NANOSECONDS);
                String duration = String.valueOf(delayMs);
                ip = String.format(RESULT, ip, duration);
                if (callbackListener != null){
                    callbackListener.callback(ip);}
            }
        }).start();
    }
}
