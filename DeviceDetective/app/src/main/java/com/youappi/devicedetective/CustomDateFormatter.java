package com.youappi.devicedetective;

/**
 * Created by boris on 2/18/2018.
 * Helper class to convert Google TimeStamps
 */

public class CustomDateFormatter {

    public static String getFormattedValue(Long value){
        if (value != 0){
            String date = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date (value*1000));
        return date;}
        return "0";
    }
}
