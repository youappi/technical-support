package com.youappi.devicedetective;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.TimeUnit;

import static com.youappi.devicedetective.Constants.NOT_AVAILABLE;
import static com.youappi.devicedetective.Constants.RESULT;

/**
 * Created by boris on 2/5/2018.
 * Service class to fetch user's data incl. country, language, carrier, install referrer and device name.
 */

public class DataService {
    private final Context context;

    public DataService (Context context){this.context = context;}

    /*
    Get device's country.
     */
    public String getCountry(){
        final long startTime = System.nanoTime();
        String country = NOT_AVAILABLE;
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                country = simCountry.toUpperCase();
            }
            else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    country = networkCountry.toUpperCase();
                }
            }
        }
        catch (Exception e) { }
        if (country.equalsIgnoreCase(NOT_AVAILABLE)){// there's no sim card or carrier is offline
            country = context.getResources().getConfiguration().locale.getCountry();}
        Log.w("COUNTRY", "Country="+ country);
        final long delayNano = System.nanoTime() - startTime;
        int delayMs = (int) TimeUnit.MILLISECONDS.convert(delayNano, TimeUnit.NANOSECONDS);
        String duration = String.valueOf(delayMs);
        return String.format(RESULT, country, duration);
    }

    /*
    Get device's default language.
     */
    public String getLanguage(){
        final long startTime = System.nanoTime();
        String language = context.getResources().getConfiguration().locale.getDisplayLanguage();
        if (language == null){language = NOT_AVAILABLE;}
        Log.w("LANGUAGE", "Language="+ language);
        final long delayNano = System.nanoTime() - startTime;
        int delayMs = (int) TimeUnit.MILLISECONDS.convert(delayNano, TimeUnit.NANOSECONDS);
        String duration = String.valueOf(delayMs);
        return String.format(RESULT, language, duration);
    }

    /*
    Get the name of user's carrier.
     */
    public String getCarrier(){
        final long startTime = System.nanoTime();
        String carrier = "";
        TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager != null) {carrier = manager.getNetworkOperatorName();}
        //If there's no sim card in the phone getNetworkOperatorName() returns empty string
        if (carrier.equalsIgnoreCase("")){carrier = NOT_AVAILABLE;}
        Log.w("CARRIER", "Carrier="+ carrier);
        final long delayNano = System.nanoTime() - startTime;
        int delayMs = (int) TimeUnit.MILLISECONDS.convert(delayNano, TimeUnit.NANOSECONDS);
        String duration = String.valueOf(delayMs);
        return String.format(RESULT, carrier, duration);
    }

    /*
    Get device name.
     */
    public String getDeviceName() {
        String deviceName = "";
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            deviceName = capitalize(model);
        }
        deviceName = capitalize(manufacturer) + " " + model;
        Log.w("DEVICE NAME", "DEVICE NAME="+deviceName);
        return deviceName;
    }

    //Helper method to get the market name of Android device
    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }
        return phrase.toString();
    }
}