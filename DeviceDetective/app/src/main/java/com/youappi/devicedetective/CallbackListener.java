package com.youappi.devicedetective;

/**
 * Created by boris on 2/8/2018.
 * Helper interface to send callbacks;
 */

public interface CallbackListener {
    void callback(String response);
}
