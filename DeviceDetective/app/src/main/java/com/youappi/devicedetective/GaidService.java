package com.youappi.devicedetective;

import android.app.Activity;
import android.util.Log;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.youappi.devicedetective.Constants.RESULT;

/**
 * Created by boris on 2/8/2018.
 * Service class to fetch the user's Advertising ID.
 */

public class GaidService {

    private String gaid;
    public CallbackListener callbackListener;

    public void setCallbackListener(CallbackListener callbackListener){
        this.callbackListener = callbackListener;
    }

    /*
 Fetch Google Advertising ID of the device.
 According to Google documentation it runs the separate thread and is not called in the main function.
  */
    public void getGAID(Activity a) {
        final long startTime = System.nanoTime();
        final Activity activity = a;
        Log.v("GAID", "Trying to get Advertisement ID from Google Play Services");
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        if (googleAPI.isGooglePlayServicesAvailable(activity) == ConnectionResult.SUCCESS) {
            new Thread(new Runnable() {
                public void run() {
                    AdvertisingIdClient.Info adInfo = null;
                    try {
                        adInfo = AdvertisingIdClient.getAdvertisingIdInfo(activity);
                    } catch (IOException e) {
                        Log.e("GAID", e.getMessage());
                    } catch (GooglePlayServicesNotAvailableException e) {
                        Log.e("GAID", e.getMessage());
                    } catch (GooglePlayServicesRepairableException e) {
                        Log.e("GAID", e.getMessage());
                    }
                    gaid = adInfo.getId();
                    Log.w("GAID", "GAID=" + gaid);
                    boolean userOptOutAdTracking = adInfo.isLimitAdTrackingEnabled();

                    // Respecting user’s “opt out of interest-based advertising” setting
                    // More details in: https://play.google.com/intl/ALL_us/about/developer-content-policy.html
                    if (userOptOutAdTracking) {
                        Log.v("GAID", "Removing Advertisement ID from Prefs");

                    } else {
                        Log.v("GAID", "Saving Advertisement ID: " + gaid + " to Prefs");
                    }
                    final long delayNano = System.nanoTime() - startTime;
                    int delayMs = (int) TimeUnit.MILLISECONDS.convert(delayNano, TimeUnit.NANOSECONDS);
                    String duration = String.valueOf(delayMs);
                    gaid = String.format(RESULT, gaid, duration);
                    if (callbackListener != null){
                        callbackListener.callback(gaid);}
                }
            }).start();
        } else {
            Log.e("GAID", "Error: Google Play Services are not available, error: " + GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE);
        }
    }
}
