package com.youappi.devicedetective;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;

import static com.youappi.devicedetective.Constants.ALLDATA;
import static com.youappi.devicedetective.Constants.NOT_AVAILABLE;
import static com.youappi.devicedetective.Constants.REFERRER_FORMAT;
import static com.youappi.devicedetective.Constants.SIGNATURE;

public class MainActivity extends AppCompatActivity implements InstallReferrerStateListener {

    private TextView referrerText;
    private TextView ipText;
    private TextView gaidText;
    private String deviceName;
    private String language;
    private String carrier;
    private String country;
    private String gaid;
    private String ip;
    private String referrer;
    private InstallReferrerClient mReferrerClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView deviceText = (TextView) findViewById(R.id.textView);
        TextView languageText = (TextView) findViewById(R.id.textView2);
        TextView carrierText = (TextView) findViewById(R.id.textView3);
        TextView countryText = (TextView) findViewById(R.id.textView4);
        ipText = (TextView) findViewById(R.id.textView5);
        gaidText = (TextView) findViewById(R.id.textView6);
        referrerText = (TextView) findViewById(R.id.textView7);
        Button myButton = (Button) findViewById(R.id.button);
        Context context = getApplicationContext();
        final DataService dataService = new DataService(context);

        GaidService gaidService = new GaidService();
        gaidService.setCallbackListener(new CallbackListener() {
            @Override
            public void callback(String response) {
                gaid = response;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gaidText.setText(gaid);
                    }
                });
            }
        });
        gaidService.getGAID(this);

        IPService ipService = new IPService();
        ipService.setCallbackListener(new CallbackListener() {
            @Override
            public void callback(String response) {
                ip = response;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ipText.setText(ip);
                    }
                });
            }
        });
        ipService.getIP();

        deviceName = dataService.getDeviceName();
        language = dataService.getLanguage();
        carrier = dataService.getCarrier();
        country = dataService.getCountry();
        deviceText.setText(deviceName);
        languageText.setText(language);
        carrierText.setText(carrier);
        countryText.setText(country);
        mReferrerClient = InstallReferrerClient.newBuilder(this).build();
        mReferrerClient.startConnection(this);

        // Capture button clicks
        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View Args0) {
                String all = String.format(ALLDATA, deviceName, language, carrier, country, ip, gaid, referrer, SIGNATURE);
                shareData(all);
            }
        });
    }
   /*
 Process the result of using Install Referrer Client
 */
    @Override
    public void onInstallReferrerSetupFinished(int responseCode) {
       Log.w("Install Referrer Client" , "onInstallReferrerSetupFinished: responseCode=" + responseCode);
        if (responseCode == InstallReferrerClient.InstallReferrerResponse.OK) {
            try {
                final ReferrerDetails response = mReferrerClient.getInstallReferrer();
                referrer = response.getInstallReferrer();
                Log.w("Install Referrer Client", "onInstallReferrerSetupFinished: InstallReferrer=" + referrer);
                String installBeginTimestamp = CustomDateFormatter.getFormattedValue(response.getInstallBeginTimestampSeconds());
                String referrerClickTimestamp = CustomDateFormatter.getFormattedValue(response.getReferrerClickTimestampSeconds());
                referrer = String.format(REFERRER_FORMAT, referrer, installBeginTimestamp, referrerClickTimestamp);
                referrerText.setText(referrer);
                mReferrerClient.endConnection();
            } catch (RemoteException e) {
                Log.e("Install Referrer Client", e.getMessage());
            }
        }
    }

    // If failed to receive install referrer because of disconnection
    @Override
    public void onInstallReferrerServiceDisconnected() {
        Log.e("REFERRER", "Failed to receive referrer. Reconnecting now...");
        referrer = NOT_AVAILABLE;
        referrerText.setText(referrer);
        mReferrerClient.startConnection(this);
    }

    /*
    Share the data to other apps.
     */
    private void shareData(String s){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, s);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "My " + deviceName + " details");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Sharing my smartphone's details..."));
    }
}