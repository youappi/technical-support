/**
 * Created by Boris on 25.03.2018.
 */

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/*
Aggregate the result and write it to .txt file
 */
public class Aggregator {
    private List<IDMatcher> IDMatchers = new ArrayList<>();
    private Vector<String> allResults = new Vector<>();

    public void addLinkMatcher(IDMatcher IDMatcher) {
        IDMatchers.add(IDMatcher);}

    private void collectResults() {
        for (IDMatcher l : IDMatchers) {
            Vector<String> result = l.getResult();
            for (String s : result){allResults.add(s);}
        }
    }

    public void getResults() throws IOException {
        collectResults();
        try (BufferedOutputStream bout = new BufferedOutputStream(new FileOutputStream("output.txt"))){
            for (String s : allResults) {
                s += System.getProperty("line.separator");
                bout.write(s.getBytes());
            }}
    }
}