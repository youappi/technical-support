/*
Created by Boris on 26.03.2018.
 */

import java.io.IOException;
import java.net.URISyntaxException;

public class Runner {
    private static final String path = "input.txt";

    public static void main(String[] args) throws InterruptedException, IOException, URISyntaxException {
        FileProcessor fileProcessor = new FileProcessor();
        fileProcessor.processFile(path);}
}
