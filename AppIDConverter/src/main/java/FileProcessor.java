/*
* created by Boris on 25.03.2018
 */


import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/*
Read the .txt file
 */
public class FileProcessor {
    private int threadNumber = 1;
    private Aggregator aggregator = new Aggregator();
    private List<Thread> threads = new ArrayList<Thread>();
    private final int chunkSize = 1000;

    public void processFile(String path) throws IOException, InterruptedException, URISyntaxException {
        LineIterator it = IOUtils.lineIterator(new FileInputStream(path), "UTF-8");
        try {
            int counter = 0;
            String chunk ="";
            while (it.hasNext()) {
                String line = it.nextLine();
                counter++;
                chunk = String.join("\n+", chunk, line);
                //Here I check if there are already 1000 lines read.
                // If there are less than 1000 lines read, but the end of the file is already reached
                // I start a new Thread to process the last lines, too.
                if (counter == chunkSize
                        || (counter < chunkSize && !it.hasNext())) {
                    IDMatcher IDMatcher = new IDMatcher(chunk, threadNumber);
                    Thread myThread = new Thread(IDMatcher);
                    myThread.start();
                    threadNumber++;
                    threads.add(myThread);
                    aggregator.addLinkMatcher(IDMatcher);
                    counter = 0;
                    chunk = "";}
            }}
        finally {LineIterator.closeQuietly(it);}
        for (Thread t : threads) {t.join();}
        aggregator.getResults();}
}
