/**
 * Created by Boris on 26.03.2018.
 */

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

/*
Check if track ID of iOS App is valid. Provide the name / bundle ID of an app.
 */
public class IDMatcher implements Runnable {

    private final String basicURL = "https://itunes.apple.com/app/id%s";
    private final String getURL = "https://itunes.apple.com/lookup?id=%s";
    private final String resultString = "%s %s";
    private int count;
    private String chunk;
    protected Vector<String> result = new Vector<String>();

    public IDMatcher(String chunk, int count) {
        this.chunk = chunk;
        this.count = count;
    }

    public void run() {
        List<String> lines = new ArrayList<>();
        Scanner sc = new Scanner(chunk);
        sc.useDelimiter("\n");
        while (sc.hasNext()) {
            lines.add(sc.next());
        }
        for (int i = 0; i < lines.size(); i++) {
            if (i==0) {System.out.println("Thread=" + count + " is in process...");}
            if (i==(lines.size()-1)){System.out.println("Finished thread=" + count);}
            String line = lines.get(i);
            try {String link = String.format(basicURL, line.substring(1));
                if (validLink(link)) {
                    String appName = getAppName(line);
                    result.add(String.format(resultString, appName, link));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Boolean validLink(String s) throws IOException {
        URL u = new URL(s);
        HttpURLConnection huc = (HttpURLConnection) u.openConnection();
        huc.setRequestMethod("GET");
        huc.connect();
        int code = huc.getResponseCode();
        if (code == 200) {
            return true;
        }
        return false;
    }

    private String getAppName(String s){
        //to do: parse the json received from https://itunes.apple.com/lookup?id=284910350
        String lookupUrl = String.format(getURL, s.substring(1));
        RestTemplate restTemplate = new RestTemplate();
        String iTunesString = restTemplate.getForObject(lookupUrl, String.class);
        JsonObject obj = new JsonParser().parse(iTunesString).getAsJsonObject();
        JsonArray jarray = obj.getAsJsonArray("results");
        String appName = "unknown";
        for (JsonElement j : jarray) {
            JsonObject resultObj = j.getAsJsonObject();
            appName = resultObj.get("trackName").getAsString();
        }
        return appName;
    }

    public Vector<String> getResult() {
        return result;
    }
}