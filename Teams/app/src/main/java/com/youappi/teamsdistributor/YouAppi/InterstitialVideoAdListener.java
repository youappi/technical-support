package com.youappi.teamsdistributor.YouAppi;

/**
 * Created by boris on 3/20/2018.
 */

import android.util.Log;

import com.youappi.ai.sdk.YAErrorCode;
import com.youappi.ai.sdk.ads.YAInterstitialVideoAd;
import com.youappi.teamsdistributor.CallbackListener;

public class InterstitialVideoAdListener implements YAInterstitialVideoAd.InterstitialVideoAdListener {
    private CallbackListener callbackListener;

    public InterstitialVideoAdListener(CallbackListener callbackListener) {
        this.callbackListener = callbackListener;
    }

    @Override
    public void onVideoStart(String s) {
    }

    @Override
    public void onVideoEnd(String s) {
    }

    @Override
    public void onVideoSkipped(String s, int i) {
    }

    @Override
    public void onCardShow(String s) {
    }

    @Override
    public void onCardClose(String s) {
    }

    @Override
    public void onCardClick(String s) {
    }

    @Override
    public void onLoadSuccess(String adUnitId) {
        Log.i("VideoAd", "Interstitial video loaded succesfully");
    }

    @Override
    public void onLoadFailure(String adUnitId, YAErrorCode yaErrorCode, Exception e) {
        Log.e("VideoAd", "Failed loading ad unit: " + adUnitId +
                " for reason: " + yaErrorCode);
        sendCallback();
    }


    @Override
    public void onShowFailure(String adUnitId, YAErrorCode yaErrorCode, Exception e) {
        Log.e("VideoAd", "Failed showing ad unit: " + adUnitId +
                " for reason: " + yaErrorCode);
        sendCallback();
    }

    @Override
    public void onAdStarted(String s) {
    }

    @Override
    public void onAdEnded(String s) {
        sendCallback();
    }

    //Sends callback to load new ads.
    private void sendCallback() {
        if (callbackListener != null) {
            callbackListener.callback(true);
        }
    }
}
