package com.youappi.teamsdistributor;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.youappi.ai.sdk.YouAPPi;
import com.youappi.ai.sdk.ads.YAInterstitialAd;
import com.youappi.teamsdistributor.YouAppi.InterstitialAdListener;

public class DeleteActivity extends Activity implements OnCheckedChangeListener, OnClickListener, CallbackListener {
    private ArrayList<String> playerList;
    private ArrayList<UpdateService> updateList;
    private CheckBox[] ch;
    private LinearLayout linearLayout;
    private Button selectAll;
    private TextView tvTotalPlayers;
    protected YAInterstitialAd interstitialAd;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        initializeIntAdUnit(); //initialize and load YouAppi Ads
        requestWindowFeature(Window.FEATURE_NO_TITLE); //removes action bar at the top
        setContentView(R.layout.deletename);
        linearLayout = findViewById(R.id.layout_checkBoxes);
        playerList = getIntent().getStringArrayListExtra(GenerateActivity.PLAYER_LIST_EXTRA_KEY);

        if (getIntent().getSerializableExtra(GenerateActivity.PLAYER_UPDATE_LIST_EXTRA_KEY) != null) { //may return null
            updateList = (ArrayList<UpdateService>) getIntent().getSerializableExtra(GenerateActivity.PLAYER_UPDATE_LIST_EXTRA_KEY);
        }
        ch = new CheckBox[playerList.size()];
        initializeViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @SuppressLint("SetTextI18n")
    public void initializeViews() {
        for (int i = 0; i < playerList.size(); i++) {
            ch[i] = new CheckBox(this);
            ch[i].setOnCheckedChangeListener(this);
            ch[i].setId(i);
            ch[i].setText(playerList.get(i));
            ch[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.rtg_text_size));
            linearLayout.addView(ch[i]);
        }
        tvTotalPlayers = findViewById(R.id.tvDelNameTotalPlayers);
        selectAll = findViewById(R.id.bSelectAll);
        Button delete = findViewById(R.id.bDelete);
        tvTotalPlayers.setText(Integer.toString(playerList.size()));
        selectAll.setOnClickListener(this);
        delete.setOnClickListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton view, boolean isChecked) {
        for (CheckBox cb : ch) {
            //if even one checkbox is unchecked set it back to "Select All"
            if (!cb.isChecked()) {
                selectAll.setText(getString(R.string.select_all));
                return;
            }
        }
        selectAll.setText(getString(R.string.deselect_all));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bSelectAll:
                if (selectAll.getText().toString().equals(getString(R.string.select_all_str))) {
                    for (CheckBox c : ch) {
                        c.setChecked(true);
                    }
                    selectAll.setText(getString(R.string.deselect_all)); //if select all change to deselect all

                } else if (selectAll.getText().toString().equals(getString(R.string.deselect_all))) {
                    for (CheckBox c : ch) {
                        c.setChecked(false);
                    }
                    selectAll.setText(getString(R.string.select_all)); //if deselect all change to select all
                }
                break;
            case R.id.bDelete:
                int yGravity = 270;
                int gravityType = Gravity.TOP;
                Boolean fatalityBool = false;
                for (CheckBox c : ch) {
                    if (c.isChecked() && c.getVisibility() == CheckBox.VISIBLE) {
                        playerList.remove(c.getText().toString());
                        tvTotalPlayers.setText(Integer.toString(playerList.size())); //update total

                        if (updateList != null) {
                            updateList.add(new UpdateService(getString(R.string.delete_action), c.getText().toString()));//add to updatelist if updatelist was passed in to delete activity
                        }

                        linearLayout.removeView(c); //remove checkbox from view
                        c.setVisibility(View.GONE);

                        if (!fatalityBool) {
                            fatalityBool = true;
                        }
                    }
                }

                if (fatalityBool) {
                    Toast t = Toast.makeText(getApplicationContext(), getString(R.string.name_deleted), Toast.LENGTH_SHORT);
                    t.setGravity(gravityType, 0, yGravity);
                    t.show();
                }

                if (linearLayout.getChildCount() == 0) {
                    returnDataToMain();
                    finish();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        interstitialAd.show();
        returnDataToMain();
        super.onBackPressed();
    }

    public void returnDataToMain() {
        Intent returnToMain = new Intent();
        returnToMain.putStringArrayListExtra(GenerateActivity.PLAYER_LIST_EXTRA_KEY, playerList);
        if (updateList != null) {
            returnToMain.putExtra(GenerateActivity.PLAYER_UPDATE_LIST_EXTRA_KEY, updateList);
        }
        //returns information back to previous class
        setResult(RESULT_OK, returnToMain); //result code and intent
    }

    /*
    Initialize and load Interstitial Ads.
     */
    private void initializeIntAdUnit() {
        if (YouAPPi.getInstance() != null) {
            interstitialAd = YouAPPi.getInstance().interstitialAd("interstitial_ad");
            interstitialAd.setInterstitialAdListener(new InterstitialAdListener(this));
            interstitialAd.load();
        } else {
            Log.e("InterstisialAd", "YouAPPi is not initialized");
        }
    }

    @Override
    public void callback(Boolean reload) {
        if (reload) {
            interstitialAd.load();
        }
    }
}
