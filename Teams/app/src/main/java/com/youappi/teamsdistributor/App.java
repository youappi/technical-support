package com.youappi.teamsdistributor;

import android.app.Application;
import android.content.Context;

import com.youappi.ai.sdk.YouAPPi;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by boris on 3/12/2018.
 */

public class App extends Application {

    private static final String ACCESS_TOKEN = "cde72356-67f0-4885-8261-fb448d0300a6";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        YouAPPi.init(this, ACCESS_TOKEN);
    }

    public static Context getAppContext(){
        return getAppContext();
    }
}
