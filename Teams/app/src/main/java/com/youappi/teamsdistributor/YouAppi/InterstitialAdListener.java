package com.youappi.teamsdistributor.YouAppi;

import android.util.Log;

import com.youappi.ai.sdk.YAErrorCode;
import com.youappi.ai.sdk.ads.YAInterstitialAd;
import com.youappi.teamsdistributor.CallbackListener;

/**
 * Created by boris on 3/12/2018.
 */

public class InterstitialAdListener implements YAInterstitialAd.InterstitialAdListener {

    private CallbackListener callbackListener;

    public InterstitialAdListener(CallbackListener callbackListener) {
        this.callbackListener = callbackListener;
    }

    @Override
    public void onCardShow(String s) {
    }

    @Override
    public void onCardClose(String s) {
    }

    @Override
    public void onCardClick(String s) {
    }

    @Override
    public void onLoadSuccess(String adUnitId) {
        Log.d("InterstitialAd", "Card is loaded");
    }

    @Override
    public void onLoadFailure(String adUnitId, YAErrorCode yaErrorCode, Exception e) {
        Log.e("InterstitialAd", "Failed loading ad unit: " + adUnitId +
                " for reason: " + yaErrorCode);
        sendCallback();
    }

    @Override
    public void onShowFailure(String adUnitId, YAErrorCode yaErrorCode, Exception e) {
        Log.e("InterstitialAd", "Failed showing ad unit: " + adUnitId +
                " for reason: " + yaErrorCode);
        sendCallback();
    }

    @Override
    public void onAdStarted(String s) {
    }

    @Override
    public void onAdEnded(String s) {
        sendCallback();
    }

    //Sends callback to load new ads.
    private void sendCallback() {
        if (callbackListener != null) {
            callbackListener.callback(true);
        }
    }
}
