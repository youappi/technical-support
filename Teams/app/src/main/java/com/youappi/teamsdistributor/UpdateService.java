package com.youappi.teamsdistributor;

import java.io.Serializable;

class UpdateService implements Serializable {
    private String action;
    private String playerName;

    UpdateService(String action, String playerName) {
        try {
            if (action.equals(App.getAppContext().getString(R.string.add_action)) || action.equals(App.getAppContext().getString(R.string.delete_action))) {
                this.action = action;
            } else {
                throw new Exception("Update action must be \"add\" or \"delete\"");
            }
        } catch (Exception e) {
            e.getMessage();
        }

        this.playerName = playerName;
    }
}
