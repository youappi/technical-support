package com.youappi.teamsdistributor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NumberOfTeamsDialog extends DialogFragment implements OnClickListener {

    GenerateActivity tr;
    EditText etNumberOfTeams;
    private static final int yGravity = 270;
    private static final int gravityType = Gravity.TOP;

    @Override
    public void onAttach(Activity activity) { //the activity holding the fragment will be passed in here
        super.onAttach(activity);
        tr = (GenerateActivity) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        if (tr.getNumberOfTeamsPrevious() != null) { //set number of teams to the previous setting
            etNumberOfTeams.setText(tr.getNumberOfTeamsPrevious());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.enter_number_of_teams));
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.numberofteamsdialog, null);
        etNumberOfTeams = view.findViewById(R.id.etNumberOfTeams);
        Button bGoRandomize = view.findViewById(R.id.bConfirm);
        Button bCancelRandomize = view.findViewById(R.id.bCancel);
        bGoRandomize.setOnClickListener(this);
        bCancelRandomize.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bCancel:
                dismiss();
                break;
            case R.id.bConfirm:
                handleConfirmPressed();
                break;
        }

    }

    private void handleConfirmPressed() {
        try {
            int numberOfTeams = Integer.parseInt(etNumberOfTeams.getText().toString());
            if (numberOfTeams < 2) {
                showToast(R.string.please_enter_at_least_2_or_more, Toast.LENGTH_SHORT);
            } else if (numberOfTeams > tr.getPlayersArrList().size()) {
                showToast(R.string.the_number_of_teams_must_be_less_or_equal_to_the_num_of_players, Toast.LENGTH_LONG);
            } else {
                tr.setNumberOfTeamsPrevious(Integer.toString(numberOfTeams)); //lets edittext 'to remember' the previous entered number
                Intent i = new Intent(getActivity(), RandomizeActivity.class);
                i.putStringArrayListExtra(RandomizeActivity.PLAYERS_ARRAY_LIST_PARAM_KEY, tr.getPlayersArrList());
                i.putExtra(RandomizeActivity.NUMBER_OF_TEAMS_PARAM_KEY, numberOfTeams);
                startActivity(i);
                dismiss();
            }
        } catch (NumberFormatException e) {
            showToast(R.string.please_enter_at_least_2_or_more, Toast.LENGTH_SHORT);
        }

    }

    private void showToast(int stringId, int length) {
        Toast t = Toast.makeText(getActivity(), getString(stringId), length);
        t.setGravity(gravityType, 0, yGravity);
        t.show();
    }
}