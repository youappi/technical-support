package com.youappi.teamsdistributor;

/**
 * Created by boris on 3/25/2018.
 */

public interface CallbackListener {
    void callback(Boolean reload);
}
