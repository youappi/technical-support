package com.youappi.teamsdistributor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.youappi.ai.sdk.YouAPPi;
import com.youappi.ai.sdk.ads.YAInterstitialVideoAd;
import com.youappi.teamsdistributor.YouAppi.InterstitialVideoAdListener;

public class RandomizeActivity extends Activity implements CallbackListener {

    private static final String TAG = RandomizeActivity.class.getSimpleName();

    static final String PLAYERS_ARRAY_LIST_PARAM_KEY = "playerArrList";
    static final String NUMBER_OF_TEAMS_PARAM_KEY = "numberOfTeams";

    private ArrayList<String> playerList;
    private LinearLayout llTeamLeft, llTeamRight;
    private int numberOfTeams;
    private Button bRandomizeAgain;

    int numberOfClicks;
    private YAInterstitialVideoAd interstitialVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //removes action bar at the top
        setContentView(R.layout.randomize);
        playerList = getIntent().getStringArrayListExtra(PLAYERS_ARRAY_LIST_PARAM_KEY);
        numberOfTeams = getIntent().getIntExtra(NUMBER_OF_TEAMS_PARAM_KEY, 2);
        initializeViews();
        initializeVideoAdUnit();
        startRandomizeThread();
        numberOfClicks = 0;
    }

    @SuppressLint("SetTextI18n")
    public void initializeViews() {
        llTeamLeft = findViewById(R.id.llTeamLeft);
        llTeamRight = findViewById(R.id.llTeamRight);
        ((TextView) findViewById(R.id.tvRandomizeTotalPlayers)).setText(Integer.toString(playerList.size()));
        bRandomizeAgain = findViewById(R.id.bRandomizeAgain);
        bRandomizeAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberOfClicks++;
                if (numberOfClicks % 10 == 0 || numberOfClicks % 10 == 5) {
                    if (interstitialVideoAd.isAvailable()){
                        interstitialVideoAd.show();
                    }
                }
                startRandomizeThread();
            }
        });
    }

    public void randomize() {
        int team = 0;
        int playerNumCounter = 1;
        LinearLayout[] llTeam = new LinearLayout[numberOfTeams];

        for (int i = 0; i < llTeam.length; i++) {
            llTeam[i] = new LinearLayout(this);
            llTeam[i].setId(i);
            llTeam[i].setOrientation(LinearLayout.VERTICAL);

            TextView tvTeamNum = new TextView(this);
            tvTeamNum.setText(Html.fromHtml("<u><b>Team " + (i + 1) + "</b></u>"));
            tvTeamNum.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.rtg_text_size));

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
            lp.gravity = Gravity.CENTER_HORIZONTAL;

            if (i > 1) { //if not team 1 or team 2 add spacing in between teams
                lp.topMargin = 50;
            }

            tvTeamNum.setLayoutParams(lp);//set the parameters to the textView

            //adding the linearLayouts to the left scroll view and right scroll views
            if (team == 0) {
                llTeamLeft.addView(llTeam[i]);
                llTeam[i].addView(tvTeamNum);
                team = 1;
            } else if (team == 1) {
                llTeamRight.addView(llTeam[i]);
                llTeam[i].addView(tvTeamNum);
                team = 0;
            }
        }

        team = 0;
        Collections.shuffle(playerList); //randomize arrayList
        int lastPlayer = 1;
        for (int i = 0; i < playerList.size(); i++) {
            String playerName = playerList.get(i);
            TextView tvName = new TextView(this);
            if (i == playerList.size() - 1 && playerNumCounter > lastPlayer) {
                tvName.setText(Html.fromHtml("<font color=\"red\"><b>" + (Integer.toString(playerNumCounter) + ". " + playerName).replaceFirst("\\s", "\u00A0") + "</b></font>"));
            } else {
                tvName.setText((Integer.toString(playerNumCounter) + ". " + playerName).replaceFirst("\\s", "\u00A0"));
            }
            tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.rtg_text_size));
            tvName.setSingleLine();
            tvName.setEllipsize(TextUtils.TruncateAt.END);

            if (numberOfTeams > 3 && i == playerList.size() - 1 && (team + 1) % 2.0f != 0) { //if team>3, is last name in list, and need to land on odd team
                llTeam[numberOfTeams - 1].addView(tvName); //if a name makes the teams unaligned add it to the last one
            } else {
                llTeam[team].addView(tvName);
            }
            team++;
            lastPlayer = playerNumCounter;

            if (team == numberOfTeams) {
                team = 0;
                //playerNumCounter is the number in "1. Shay 2. Roee"
                playerNumCounter++;
            }
        }
    }

    private void startRandomizeThread() {
        bRandomizeAgain.setVisibility(View.INVISIBLE);
        Thread randomizeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (RandomizeActivity.this) {
                    try {
                        Random rand = new Random();
                        for (int i = 0; i < rand.nextInt(3) + 5; i++) {
                            if (i != 0) {
                                Thread.sleep(330);
                            }

                            RandomizeActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    llTeamLeft.removeAllViews();
                                    llTeamRight.removeAllViews();
                                    randomize();
                                }
                            });
                        }

                    } catch (InterruptedException e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                    } finally {
                        bRandomizeAgain.post(new Runnable() {
                            @Override
                            public void run() {
                                bRandomizeAgain.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }
            }
        });
        randomizeThread.start();
    }

    /*
    Initialize and load Interstitial Ads.
     */
    public void initializeVideoAdUnit() {
        if (YouAPPi.getInstance() != null) {
            interstitialVideoAd = YouAPPi.getInstance().interstitialVideoAd("interstitial_video");
            interstitialVideoAd.setInterstitialVideoAdListener(new InterstitialVideoAdListener(this));
            interstitialVideoAd.load();
        } else {
            Log.e("VideoAd", "YouAPPi is not initialized");
        }
    }


    @Override
    public void callback(Boolean reload) {
        if (reload) {
            interstitialVideoAd.load();
        }
    }
}