package com.youappi.teamsdistributor;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.AddFloatingActionButton;

public class GenerateActivity extends Activity implements OnClickListener {

    static final String PLAYER_LIST_EXTRA_KEY = "playerList";
    static final String PLAYER_UPDATE_LIST_EXTRA_KEY = "updateList";
    private TextView tvTotal, tvEmpty;
    private EditText etName;
    private ScrollView sv;
    private LinearLayout llPlayerList;
    private ArrayList<String> playerArrList;
    private ArrayList<UpdateService> updateList;
    private boolean changesMade;
    private int yGravity = 270;
    private int gravityType = Gravity.TOP;
    private String numberOfTeamsPrevious;
    private static boolean keyboardHidden = true;
    private int etNameYCoordinate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //removes action bar at the top

        //stops keyboard from automatically appearing and prevents layout from shifting when keyboard is visible
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.generate);
        initializeViews();
        playerArrList = new ArrayList<>();
    }

    @Override
    protected void onPause() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
        }
        super.onPause();
    }

    public void initializeViews() {
        tvTotal = findViewById(R.id.tvTotalPlayers);
        tvEmpty = findViewById(R.id.tvPlayerList);
        etName = findViewById(R.id.etPlayerName);
        sv = findViewById(R.id.svScroll);
        llPlayerList = findViewById(R.id.llPlayerList);
        final Button delName = findViewById(R.id.bDelName);
        final ViewGroup rlLabels = findViewById(R.id.rlLabels);
        AddFloatingActionButton addActionButton = findViewById(R.id.add_action_button);
        //Listener for when the user hits the enter key on softkeyboard
        etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_NULL
                        || event.getKeyCode() == KeyEvent.KEYCODE_ENTER)
                        && event.getAction() == KeyEvent.ACTION_UP) {
                    addName();
                }
                return true;
            }
        });

        //get the starting coordinates of etName before it shifts
        etName.post(new Runnable() {
            @Override
            public void run() {
                int[] coordinates = {0, 0};
                etName.getLocationInWindow(coordinates);
                etNameYCoordinate = coordinates[1];
            }
        });

        //Listener to detect if keyboard is shown or not
        final View decorView = this.getWindow().getDecorView();
        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //Calculations to detect if keyboard is present or not
                Rect rect = new Rect();
                decorView.getWindowVisibleDisplayFrame(rect);
                int displayHeight = rect.bottom - rect.top;
                int height = decorView.getHeight();
                boolean keyboardHiddenTemp = (double) displayHeight / height > 0.8;
                if (keyboardHiddenTemp != keyboardHidden) {
                    keyboardHidden = keyboardHiddenTemp;

                    if (!keyboardHidden) {
                        //keyboard shown

                        sv.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                int[] coordinates = {0, 0};
                                etName.getLocationInWindow(coordinates);
                                //beginning coordinates subtract the new shifted coordinates is the amount shifted
                                int shiftInLayoutByKeyboard = etNameYCoordinate - coordinates[1];
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlLabels.getLayoutParams();
                                params.setMargins(0, shiftInLayoutByKeyboard, 0, 0);
                                rlLabels.setLayoutParams(params);
                                sv.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        //scrolls the scrollview to the bottom
                                        sv.fullScroll(View.FOCUS_DOWN);
                                    }
                                });
                            }
                        }, 225);

                    } else {
                        //keyboard hidden
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlLabels.getLayoutParams();
                        params.setMargins(0, 0, 0, 0);
                        rlLabels.setLayoutParams(params);

                    }
                }
            }
        });

        delName.setOnClickListener(this);
        findViewById(R.id.bGenerate).setOnClickListener(this);
        addActionButton.setOnClickListener(this);
        findViewById(R.id.rlRoot).setOnClickListener(this);
        findViewById(R.id.llPlayerList).setOnClickListener(this);

        sv.setOnClickListener(this);
    }

    //addNameToScreen is called while coming back from another activity and it needs to refresh the list due to some changes
    @SuppressLint("SetTextI18n")
    private void addName() {
        //Update List
        String name;
        //get name in EditText
        name = etName.getText().toString().trim();

        int messageAssetId = 0;
        //if name is empty display toast and return
        if (name.isEmpty()) {
            messageAssetId = R.string.please_enter_a_name;
            etName.setText(""); //reset edittext
        } else if (name.contains("~") || name.contains("'")) {
            messageAssetId = R.string.name_cant_contain_the_symbols;
        } else if (playerArrList.contains(name)) {
            messageAssetId = R.string.please_enter_a_unique_name;
        } else if (name.length() > 26) {
            messageAssetId = R.string.name_must_be_1_to_26_chars_long;
        }
        if (messageAssetId != 0) {
            showToast(getString(messageAssetId));
            return;
        }

        //Update Total
        int total = Integer.parseInt(tvTotal.getText().toString());
        total++;
        tvTotal.setText(Integer.toString(total));
        if (!isChangesMade()) {
            setChangesMade(true); //set changes made to true on add
        }

        //always add to playerList
        playerArrList.add(name);
        name = Integer.toString(total) + ". " + name;
        llPlayerList.removeView(tvEmpty);
        addNameToScreen(name);
        //reset editText to blank after adding a name
        etName.setText("");
        sv.post(new Runnable() {

            @Override
            public void run() {
                //scrolls the scrollview to the bottom
                sv.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void showToast(String message) {
        Toast t = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        t.setGravity(gravityType, 0, yGravity);
        t.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_action_button:
                addName();
                break;

            case R.id.bDelName:
                if (playerArrList.size() > 0) {
                    Intent intent = new Intent(this, DeleteActivity.class);
                    intent.putStringArrayListExtra(PLAYER_LIST_EXTRA_KEY, playerArrList);
                    startActivityForResult(intent, 1); //0 is default request code
                } else {
                    showToast(getString(R.string.nothing_to_delete));
                }
                break;

            case R.id.bGenerate:
                if (playerArrList.size() >= 2) {
                    FragmentManager manager = getFragmentManager();
                    NumberOfTeamsDialog numberOfTeamsDialog = new NumberOfTeamsDialog();
                    numberOfTeamsDialog.show(manager, "NumberOfTeams"); //adds fragment to the manager
                } else {
                    Toast t = Toast.makeText(getApplicationContext(), getString(R.string.please_enter_two_players), Toast.LENGTH_SHORT);
                    t.setGravity(gravityType, 0, yGravity);
                    t.show();
                }
                break;

            case R.id.llPlayerList:
            case R.id.rlRoot:
            case R.id.svScroll:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (playerArrList.size() == data.getStringArrayListExtra(PLAYER_LIST_EXTRA_KEY).size()) {
                return;
            } //exit if playerList size did not change
            if (!isChangesMade()) {
                setChangesMade(true);
            } //set changes made to true on delete
            playerArrList = data.getStringArrayListExtra(PLAYER_LIST_EXTRA_KEY);
            if (data.getSerializableExtra(PLAYER_UPDATE_LIST_EXTRA_KEY) != null) {
                updateList = (ArrayList<UpdateService>) data.getSerializableExtra("updateList");
            }
            populatePlayerList();
        }
    }

    @SuppressLint("SetTextI18n")
    public void populatePlayerList() {
        llPlayerList.removeAllViews();

        if (playerArrList.size() == 0) {
            llPlayerList.addView(tvEmpty);
            tvTotal.setText(Integer.toString(playerArrList.size()));
            setChangesMade(false);
            return; //exit if playerArrList is 0
        }
        int i = 1;
        for (String nameString : playerArrList) {
            //add name to players list
            String name = Integer.toString(i) + ". " + nameString;
            addNameToScreen(name);
            i++;
        }
        tvTotal.setText(Integer.toString(playerArrList.size()));
    }

    public void clearAll() {
        playerArrList.clear();
        updateList = null;
        populatePlayerList();
    }

    public void addNameToScreen(String name) {
        //add name to players list
        TextView tvNewName = new TextView(this);
        tvNewName.setText(name.replaceFirst("\\s", "\u00A0")); //replace first space with unicode no break space);
        tvNewName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.rtg_text_size));
        llPlayerList.addView(tvNewName);
    }

    public void setChangesMade(boolean changesMade) {
        this.changesMade = changesMade;
    }

    public boolean isChangesMade() {
        return changesMade;
    }

    public ArrayList<String> getPlayersArrList() {
        return playerArrList;
    }

    public String getNumberOfTeamsPrevious() {
        return numberOfTeamsPrevious;
    }

    public void setNumberOfTeamsPrevious(String numberOfTeamsPrevious) {
        this.numberOfTeamsPrevious = numberOfTeamsPrevious;
    }
}